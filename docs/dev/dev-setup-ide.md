# PyCharm setup help

Using pycharm (ideally professional, totally worth it!) may make things a lot easier for you.
It is recommended to have one project per repository and opening windows as needed.
In the following, we'll provide a few screenshots to help you get started.

## Tips and tricks
* ++ctrl+alt+shift++ keyboard shortcut for code reformatting 
  (following the code style defined in pycharm; make sure you set up the code style accordingly)
* 2x++shift++ open a file
* ++ctrl+shift+f++ find in files

In the settings (++ctrl+alt+s++) you can edit the `Editor > Code Styles` for all supported languages 
(in our case mainly: Python, JavaScript, TypeScript).
It is worth taking the time to set this up properly, as you'll get styling hints or have pycharm take care of
all styling for you (e.g. correct and consistent spacing).
![Editorconfig](./img/pycharm/editorconf.png){ loading=lazy }

## Setting up Run configurations  

To set-up run configurations in pycharm follow this image
![set-up run configs](./img/pycharm/set-run-configs.png){ width=400  loading=lazy }

Under `Current File` one can define custom 
run configurations. Chose `+`-> `Python` and make sure to switch in the first line from `Script name`to `Module name`.

### `nacsos-core`
=== "Hypercorn server"
    ![Hypercorn-pipelines](./img/pycharm/set-core-hyper.png){ loading=lazy }
=== "Mypy"
    ![Mypy](./img/pycharm/set-core-mypy.png){ loading=lazy }
=== "Flake8"
    ![Flake](./img/pycharm/set-core-flake.png){ loading=lazy }

### `nacsos-pipelines`
=== "Hypercorn server"
    ![Hypercorn-pipelines](./img/pycharm/set-pipelines-hyper.png){ loading=lazy }
=== "Mypy"
    ![Mypy](./img/pycharm/set-pipelines-mypy.png){ loading=lazy }
=== "Flake8"
    ![Flake](./img/pycharm/set-pipelines-flake.png){ loading=lazy }

Setting the environment variables (`PYTHONASYNCIODEBUG`, `PYTHONOPTIMIZE`, `NACSOS_CONFIG`) is critical!

### `nacsos-data`
=== "Mypy"
    ![Mypy](./img/pycharm/set-data-mypy.png){ loading=lazy }
=== "Flake8"
    ![Flake](./img/pycharm/set-data-flake.png){ loading=lazy }

## Connection to database (optional)
By setting up a database connection, pycharm will check SQL statements in the code for you.
You'll also get a query console ++ctrl+shift+f10++ (great for testing queries) and you can look at the data (schema and actual tables/rows).

=== "Step 1"
    <div class="result" markdown>
    ![DB Connection – Step 1](./img/pycharm/db-01.png){ align=left width=400 loading=lazy }
    Open the Database tool window and select to add new Postgres data source.
    </div>
=== "Step 2"
    <div class="result" markdown>
    ![DB Connection – Step 2](./img/pycharm/db-02.png){ align=left width=400 loading=lazy }
    Enter the connection details and save. You may have to install/select a driver first. 
    It is recommended to test the connection before continuing. 
    </div>
=== "Step 3"
    <div class="result" markdown>
    ![DB Connection – Step 3](./img/pycharm/db-03.png){ align=left width=400 loading=lazy }
    Expand the list of tables to reveal schema definitions (and indices,...). 
    Double click on table to see data.
    </div>

