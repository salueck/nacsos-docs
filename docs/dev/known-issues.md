# Known issues and how to resolve them
There are some common pitfalls that you may encounter when working with the platform code and/or frameworks.
This page contains a list of errors you may come across and how to resolve them.

## Object not JSON-serialisable
Some database schemas contain `jsonb` columns, which means that you can store "any" Python dict as a json-encoded 
string and the database will know how to deal with it. On query time, SQLAlchemy will respond also with the
resolved object (i.e. converts the string to a dict for you).    
Internally, we use `pydantic` to describe how these dicts are supposed to "look" like. It might happen, that you
want to store the pydantic model directly and on `session.commit()`, you may get an error message that looks/contains 
something like that:

```
[...]
TypeError: Object of type AnnotationSchemeLabel is not JSON serializable
[...]
StatementError: (builtins.TypeError) Object of type AnnotationSchemeLabel is not JSON serializable
[SQL: INSERT INTO annotation_scheme (annotation_scheme_id, project_id, name, description, labels) VALUES (%(annotation_scheme_id)s::UUID, %(project_id)s::UUID, %(name)s, %(description)s, %(labels)s)]
[...]
```

What happened here is, that the engine tried to transform the object (i.e. pydantic model) to a JSON string but 
failed to do so (even though, pydantic implements JSON serialisation...). Consider the following example:

```python
from nacsos_data.models.annotations import AnnotationSchemeLabel
from nacsos_data.db.schemas.annotations import AnnotationScheme
label1 = AnnotationSchemeLabel(
    name = 'Relevant',
    key = 'r_1234',
    hint = 'Mark whether this is relevant or not',
    max_repeat = 1,
    required = True,
    kind = 'bool' 
)

anno_scheme = AnnotationScheme(
    project_id = pm.project_id,
    name = 'Policy Instruments',
    labels = [label1] 
)
session.add(anno_scheme)
session.commit()
```

A quick way to resolve this issue is to convert the pydantic model (`label1`) to a dict before saving:

```python hl_lines="4"
anno_scheme = AnnotationScheme(
    project_id = pm.project_id,
    name = 'Policy Instruments',
    labels = [label1.dict()] 
)
session.add(anno_scheme)
session.commit()
```