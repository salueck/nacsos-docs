# Resolving (conflicting) annotations

There should be a corresponding page in the user guide section.
Here, we focus on the technical aspect, all reasoning beyond technical/schema definition things should be 
described in the user guide.

* algorithms here: https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-data/-/tree/master/src/nacsos_data/util/annotations
* user can trigger algorithm via interface, result then stored as `BotAnnotation` (`BotKind.RESOLVE`)
* link to user guide on that topic (this should contain the actual workflow)
* "normal" export should not materialise and keep things virtual for simplicity
* how to handle the case: consolidation run and stored, new annotations come in

## Further reading
* [Schema definitions for Annotations](../schema/50_annotations.md)
* [Schema definitions for BotAnnotations](../schema/50_annotations.md)