# Welcome to NACSOS

This documentation has two parts: 
One written for users of the platform and one to document design decisions and development details for future reference.

:construction: **This is work in progress!** :construction_site:   
When you are finished reading, we may have already changed the documentation, added things, 
or the documentation may not reflect what the platform is actually doing.
We'll do our best to keep things in sync and up to date.

For a quick overview, you might be interested in [this slide deck](overview_slides.pdf) we created for the WWCS Workshop in Potsdam in Oct 2023.
