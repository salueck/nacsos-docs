# Loading data into the platform

The `nacsos_data` library currently supports imports from
* Web of Science "Plain text file" exports
* OpenAlex 
  * JSONl files, each line a document from our solr mirror
  * Solr exporter that downloads documents matching a query
* Scopus
  * CSV *(preferred)* 
  * RIS *(note that RIS is not well-defined and is missing fields)*
* Twitter
  * Query downloader
  * Query uploader
* Native JSONl: you can upload files that are already in the correct format of `AcademicItemModel`, `TwitterItemModel`, or `GenericItemModel`.
* NACSOS-legacy helper functions for transferring documents and annotations. Needs customisation per project, not recommended!

## Importing Scopus
At this point, there is no automated way to import directly from Scopus.
Once you have finalised your query, you can use the "Export" button at the top of the results table in Scopus to download results, 20,000 at a time. 

![Scopus export settings](./img/scopus.png){ width=400  loading=lazy }

Please make sure to select all fields as shown above.
If you do not explicitly need it, don't select to include references, as this massively increases the filesize and is currently not supported by the platform anyway.
Once you click "export", it takes a moment to get the file ready.

At this point, you cannot upload the export directly through the NACSOS web interface.


??? "Script for importing scopus"
    ```python
    import asyncio
    
    from nacsos_data.db import get_engine_async
    from nacsos_data.util.academic.scopus import read_scopus_csv_file
    from nacsos_data.util.academic.importer import import_academic_items
    
    
    async def main() -> None:
        # The project to import data to
        PROJECT_ID = '???'
        # The user this import will be attached to
        USER_ID = '???'
        
        # Name of the import
        IMPORT_NAME = '???'
        # Description of the import (e.g. the query used and when it was downloaded)
        IMPORT_DESC = '???'
    
        db_engine = get_engine_async(conf_file='/path/to/remote.env')
        scopus_works = list(read_scopus_csv_file('/path/to/scopus.csv',
                                                 project_id=PROJECT_ID))
        await import_academic_items(items=scopus_works,
                                    db_engine=db_engine,
                                    project_id=PROJECT_ID,
                                    import_name=IMPORT_NAME,
                                    description=IMPORT_DESC,
                                    # you can set this if you rather want to 
                                    # add new documents to an existing import
                                    import_id=None,
                                    user_id=USER_ID,
                                    # Set this to True to simulate the import without actually doing it
                                    dry_run=False,
                                    # Set these to False if you don't want to overwrite values of duplicates
                                    trust_new_authors=True,
                                    trust_new_keywords=True)
    
    
    if __name__ == '__main__':
        asyncio.run(main())
    ```

## Importing Web of Science
There is no direct import from Web of Science on the new platform, yet.
For the vast majority of projects, the time spent on downloading a few files far outweighs the time spent on constantly fixing and maintaining a scraper. 

Once you downloaded the "plain text file" exports, you can upload those to the platform.
You can either use the script above and switch out the reader to `from nacsos_data.util.academic.wos.read_wos_file`, or using the web interface.

![Scopus export settings](./img/import_interface.png){ width=400  loading=lazy }

1. Provide a name and description, click save
2. Select the exported file, click upload and then save
3. Click "initiate import"

At this point, there is no option to bulk-upload files (unless you use a script).

On the bottom of the page you can track the progress, the stats can be refreshed repeatedly to see how many items are already imported.

## Importing from OpenAlex
You can access our self-hosted OpenAlex instances (Solr and Postgres) directly.
For more information on how to write queries, please consult the [respective page in the document](./openalex.md).

You can use the Solr full-text search via the platform:

![OpenAlex query interface](./img/openalex_query.png){ width=400  loading=lazy }

Unless you know exactly what the settings mean, just use the defaults.
The +20/-20 next to the "Offset" field is effectively the pagination, but it does not work well for high page counts.
You can get a histogram (number of papers per year) by checking the box and clicking "Query".

Sometimes, there might be an error after 30s. This means, that the query was cancelled because it was too slow.
You may try it once or twice more and get lucky.

![OpenAlex query interface](./img/openalex_tokens.png){ width=400  loading=lazy }

As mentioned in the [Solr documentation](./openalex.md), it is always best practice to avoid wildcard queries and instead use an explicit `OR` clause.
By clicking on "Tokens" under the query box, you get a small tool to find candidates for a postfix-wildcard.

## Duplicates
Our goal is to keep each project free of duplicate publications.
We do not try to link publications across the platform.
During import, when using the proper functions (e.g. `import_academic_items`), we try to only insert new publications and keep track of duplicates.
For further information on that, please [read this](../../dev/practices/import_dedup.md) or the source code in the `nacsos_data` library.
