The data annotation on the web platform uses the following basic concepts:

* **Annotation Scheme:** Describes how each document should be annotated with hints and all the (hierarchical) labels and options.  
                         For more details, [check the guide](./scheme.md).
* **Assignment Scope:** A batch of documents that are assigned to users. 
                        Each annotation scheme can have multiple scopes, e.g. rounds of annotations.
                        [This guide](./assignments.md) contains notes on how to assign documents to users.  
* **Assignment:** An _assignment_ is the link between a document, annotation scheme, and user.
                  It is used to task a user to annotate this document with this annotation scheme.
                  If you create assignments via a new assignment scope, it will generate these assignments for you based on the assignment strategy you chose.
                  For machine-learning-assisted scopes, these will be created continuously or deleted/updated if a better order of operation was determined by the model.
                  An assignment has the status _FULL_ if all required annotations for this assignment are done, _PARTIAL_ if some are done, or _OPEN_ if the user has not worked on that assignment yet.
* **Annotation** A single key/value pair (e.g. "Technology" -> "BECCS") with a reference to project, document, annotation scheme, user, and assignment scope.
                 When you annotate a document with a more complex annotation scheme, this will create multiple annotations in the database (one for each label).
                 The [Guide for Annotators](./annotation.md) provides an overview of the user interface for annotators. 