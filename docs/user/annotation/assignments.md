Users with the appropriate permission see another "Annotation" entry in the navigation menu with a gear icon.
This guide walks you through the process of assigning documents to be annotated by users.

!!! warning "Save before assignment"
       In theory, you should not be able to make assignments before you saved the configuration.
       Just to be on the save side, always click save on the top right before you trigger the generation of assignments.

## Schemes and scopes screen

In order to create assignments for an annotation scheme, click the respective _":fontawesome-regular-square-plus: Add scope to ..."_ button.
For editing or viewing an existing scope, click on the respective :fontawesome-solid-screwdriver-wrench: icon.

!!! tip "Use concise and descriptive titles!"
        It might be tempting to not provide titles or leave them at the default value.
        For your own sanity (and that of colleagues), please take some time to come up with a descriptive and concise title for each assignment scope.
        For example "Test run for training students".
        Furthermore, the scope description is shown in the [annotation interface](annotation.md), thus you may want to provide what annotators should look out for specifically in this assignment scope.
        The description can also be used to keep notes on this scope for yourself in order to keep the title more concise.

![AnnoConfig screen](img/assignment_01.png){ width=400  loading=lazy }

## Assignment scope config screen
The configuration screen has three sections.
In the top section, you can specify a title and description for this scope.
In the middle section, you can create a user pool, which is already part of the configuration of the assignment strategy (see below).
In the bottom section, you can select an assignment strategy and configure its parameters.
Once the scope has assignments, another section is added where you can monitor the progress.

In the subsections below, you will find the descriptions for each assignment strategy.

![Assignment screen overview](img/assignment_02.png){ width=400  loading=lazy }

The user pool is used by some assignment strategies to know which users to assign documents to annotate to.
The list of available users is not loaded by default, so you have to click on _":fontawesome-solid-arrows-down-to-people: Load list of users"_ first.
With the :fontawesome-solid-user-plus: and :fontawesome-solid-user-minus: icons, you can add and remove users from the pool.

![User pool](img/assignment_pool.png){ width=400  loading=lazy }


### Random assignment strategy
In this strategy, a random set of documents from the project is assigned randomly to users in the pool.
The four main parameters are:

* **Number of items:** The number of documents you want annotations for. 
* **# multi-coded items** A number between zero to number of items. This indicates for how many of the documents you want to assign for annotation to more than one user. 
* **Min. # coders per item** In case you have a non-zero number of items for multi-coding, how many users should **at least** annotate each a document.
* **Max. # coders per item:** In case you have a non-zero number of items for multi-coding, how many users should **at most** annotate each a document.
* **Random seed:** If you know what this mean, you know how to use it. If not, you can just add in any number you like or leave it as is.

![Random assignment strategy](img/assignment_random_detail.png){ width=400  loading=lazy }

An example: If you want to have users annotate 20 documents and you don't need multi-coding, set values to 20, 0, 1, 1.

### Custom assignment strategies
You may also create assignments from a script or by hand via the `nacsos_data`-library (or directly in the database).
In that case, create a new assignment scope first and use the respective UUID as a reference in the assignments you create elsewhere.

## Assignment scope progress / status
This section allows you to monitor the progress and see how assignments to documents is spread across users.
Not all details are loaded by default, because it could potentially contain a lot of information.
Note, that the detail view is work-in-progress and currently more targeted for smaller sets of documents (fewer than 20).

Each row in the table corresponds to a user from the pool.
Each column corresponds to a document that has at least one assignment in this scope.
An empty (white) cell indicates no assignment for that user-document pair.
A blue cell indicates an open (unfulfilled) assignment, yellow a partially fulfilled assignment, and green a completed assignment.
Above that table, you get a summarised statistic of all assignments in this scope.

![Annotation progress](img/assignment_status.png){ width=400  loading=lazy }

## Further notes
The database schema and logic behind it does not _require_ any assignments for an annotation.
For example, you could just import annotations from elsewhere and don't need to create a respective assignment first.
Consider this to be an information for advanced usage.

It is safe to edit the title and description even after assignments have been made.
Theoretically, changing the configuration should be disabled, but if it isn't for some reason, do not edit those to prevent inconsistencies!