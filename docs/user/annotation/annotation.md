Annotators receive a set of assignments, which can be interpreted as requests to annotate a certain document.
These assignments are usually done in batches (so-called _scopes_).

## Assignment overview
On the **Annotation** overview screen, you see all the scopes of this project that have assignments for you.
Each entry in the list also shows the number of fully and partially annotated documents as well as the total number (bold).
By clicking on the tags icon, you jump straight in to the annotation screen.

![Assignments overview](img/annotation_scopes.png){ width=400  loading=lazy }

## Document annotation screen
The annotation screen shows the document to annotate in the centre, in the screenshot a tweet.
The indicators at the top **(A)** can be used to jump to the different documents in this assignment scope.
Green indicators are used for fully annotated documents, yellow means you still have work to do, blank means you haven't looked at it yet.
The arrow for (A) points at the currently open document.
If you click on an indicator, the current annotation is saved and the respective assignment is opened.
On the right hand side, you can see the annotation **sidebar** / annotation panel.
You can adjust the width with the arrows at the bottom **(D)**.
**(B)** provides a description of what to pay attention to.
You can hide/show toggle this description as needed.
Below the description, you see all the labels.
Once you are done with a document, click "Save & next".

![Annotation screen overview](img/annotation_screen_notes.png){ width=400  loading=lazy }

## Types of Labels
At this point, there are three types of labels: _boolean_ (yes/no), _single_ (single-choice option from a list), and _multi_ (multiple-choice option from a list).

* The _boolean_, in the example the relevance label, is initially in the middle position to indicate that no choice has been made.
  It will turn green (yes/true) or red (no/false) by (repeatedly) clicking the toggle switch.
* The _single-choice_ label offers multiple choices to choose from, e.g. "Global warming is not happening" and "Human greenhouse gases are not causing global warming" for the "Causes" label.
  You can select exactly one of the two choices/options by clicking on the circle or the text.
  There might be hints to further explain the meaning of the choice.
  If one exists, it will pop up as you hover the mouse pointer over the text.
* The _multi-choice_ label works the same way, but you can select more than one choice at the same time.

Not all labels are required, so the assignment will be validated as complete ("full") as soon as all required labels are annotated.
If you want to remove the choice(s) you already made, you can click on the respective delete button **(F)** to clear the annotation.

![Filled annotation example](img/annotation_screen_annotated.png){ width=400  loading=lazy }

Note, that annotation schemes can have hierarchical labels.
In the example above, each claim has sub-claims.
In the example of a multiple choice label below, choice 1 and choice 3 have sub-labels as well that are revealed when the respective option is selected.

![Multiple choice example](img/annotation_screen_multi.png){ width=400  loading=lazy }

## Repeated labels
Some labels can be repeated, i.e. added more than once to a document.
This is useful to indicate that, for example, a paper is _primarily_ on the technology "BECCS" and _secondarily_ mentions "Biochar".
Based on the use-case, this ranking might be preferred over multiple-choice labels, where all choices are "equal".
If you can repeat a label, this is indicated by a number next to it's name and a small "copy" icon **(E)**.
By clicking on that, you add a secondary, tertiary, ... label of that kind.
Note, that the counter changes for each repeat.
If you want to remove one again, you can click on the _waste bin_ icon.

![Repeated labels example](img/annotation_screen_repeat.png){ width=400  loading=lazy }

## Progress indicator bar
As described above, you can use the indicators at the top to track your progress and jump between assigned documents.
With many assignments in a single scope, this can become too hard to read and navigate.
In that case (>100 assignments/scope), the bar is split in two.
The top half is a smaller version of the bar you already know with all assignments in this scope.
There is a vertical dotted line for every 10 assignments to provide some orientation.
Below, you see the normal indicators for a subset of up to 100 assignments.
The range in the overall set of assignments is indicated by the bar in the top bar. 

![Progress indicators](img/progress.png){ width=400  loading=lazy }

### Colour settings
In some settings, you may prefer to use a different colour scheme for the progress bar.
This configuration is stored locally (temporarily) in your browser.
You can change it by clicking the gear icon left of the progress bar.

![Progress indicator gear](img/annotation_screen_gear.png){ width=100  loading=lazy }

The default is to display completed assignments as green, partially completed as yellow, and assignments without annotations are left blank.

![Progress indicator settings](img/annotation_screen_colours.png){ width=400  loading=lazy }

You can choose any label you like.
Boolean labels will appear as red (false), green (true), or white (no label).
Single labels have over 20 colours, the first three are red (0), yellow (1), and green (2).
Take note of this convention when designing your annotation scheme and choosing values per label.

### Numbering
When hovering an indicator, you may notice the numbers that appear.
These may sometimes be out of order, since the numbering follows a "global" order across all assignments and users per scope.
For example, the second document for you might be the 12th for another user, but we display the same number for the same document for all users.
Note, that the numbers are only unique and matching per scope, not across scopes or the entire project.
Only the `item_id`[^1] is a unique identifier across the entire platform.
The order in which the assignments are displayed to you and that is automatically followed, is the order that the algorithm decided you should take.

[^1]: You can find the `item_id` by clicking on the `?` to reveal the raw data.

## Keyboard bindings
You may prefer to lean back and annotate your data on the big screen.
In that case, it might be more relaxing to use the keyboard instead of the mouse for annotation.

**Moving between assignments** via ++left++/++right++ or vim (++h++/++l++) or gaming (++a++/++d++) equivalents; 
this will automatically save when there were changes to the annotation. Do not bulk-skim too quickly through assignments, this may result in misalignment. You'll notice that you are too fast when you get a "saved" notification without changing the annotation.
In fact, annotations are only saved after moving between assignments.

**Moving between labels** (e.g. selecting a label) via ++up++/++down++ or vim (++j++/++k++) or gaming (++w++/++s++) equivalents.
The name of the currently selected label will be red.
When opening an assignment page, nothing is selected until you first push the down.

**Setting a value** for the selected label via pressing a number (0-9, also on numpad).
Note, that at this point we only support single-digit values, so you can't use the keyboard to annotate value 10 and beyond.
Boolean labels (yes/no) are mapped to ++0++ (no) and ++1++ (yes).
"no" is also mapped to ++2++, as it feels intuitive to use keys that are next to one another on the keyboard.

**Add secondary label** of the currently selected label by pressing ++plus++.

**Clear the value** of the currently selected label by pressing ++backspace++.

## Further notes and known issues
Your annotations are only saved if you click on save (and you get a green confirmation notification).
They are implicitly saved when you click on the assignment indicators.
They are **not** saved the moment you select an option for a label.

You do not have to stick to the order of the assignments.
We might, however, ask you to stick to the pre-determined order, for example when we employ prioritised screening, as there is actually some meaning to it.

If you ever get an error or something looks weird, try to reload the page.
If that doesn't help, [log out and log in again](https://www.youtube.com/watch?v=5UT8RkSmN4k).
If that doesn't help, contact the project lead.