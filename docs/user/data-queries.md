# NACSOS Query Language
Across the platform, you can utilise our NQL to filter for specific items.
At this point, the filters are limited to academic projects (no twitter, generic, or patents).

## Query Grammar
For exact and most recent NQL definition, the [source code](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-data/-/blob/master/src/nacsos_data/db/crud/items/query/parse.py) is probably the best place to look.
Note, that NQL is still in development, so the syntax might still evolve, execution logic may change, and not all language features may work.

There are attribute filters for `title:`, `abstract:`, `doi:`, and `year:`, as well as filters for `import:` (filtering which Import an item matches) and `annotation`s.
Filters can be arbitrarily combined with `AND` and `OR`.
Ample use of parentheses is highly recommended, as the logic is not well-defined (mostly left-to-right) otherwise.

* The **title and abstract filters** can be combined with boolean string search.
  At this point, this is not fully supported yet, only single keywords are matched, 
  for example `title: "climate change"` is translated to SQL as `lower(academic_item.title) LIKE '%lower(climate change)%`.
  Later, you can use a combination of `AND`, `OR`, `"exact phrases"`, `keyword`, `-|~|NOT` and parentheses.
* The **DOI filter** takes a single quoted string, e.g. `doi: "10.1088/1751-8113/44/49/492001"`
* The **publication year filter** takes a boolean clause of numbers with comparators (`>`,`>=`,`<`,`<=`,`=`,`!=`), e.g. `PY: (>1990 AND < 2000) OR >2010`
* The **import filter** takes a boolean clause of import IDs, e.g. `import: (<id1> or <id2>) AND NOT <id3>`, whereas `<id>` is the UUID (formatted with or without dashes) of an import.
* The **annotation filter**, takes three arguments and some optional arguments:    
  `annotation(<key>, <type>, <value> [, <repeat>] [, <users>] [, <schemas>] [, <scopes>])`, where
    * the order of arguments can not be changed, but optional arguments (indicated by `[..]`) can be left out.
    * `<key>` is the key used in the schema definition (quoted with single quotes or unquoted)
    * `<type>` is one of `H` (human annotations), `R` (resolved annotations), or `B` (bot annotations)
    * `<value>` is 
        * `true` or `false` for boolean labels
        * **integer** clause (integer preceded by one of `>`,`>=`,`<`,`<=`,`=`, or `!=`)
        * **float** clause (float preceded by one of `>`,`>=`,`<`,`<=`,`=`, or `!=`; float values always have to contain a ".", so 2.0 cannot be written as 2!)
        * **multi**-select search is not implemented yet
    * `<repeat>` is a boolean integer clause
    * `<users>` is a comma-separated list of user ids wrapped in `[` and `]`. You can use `ALL users IN [id1, id2, id3]` to indicate that all of those users have to have made this annotation value or `ANY user IN [id1, id2, id3]` to require at least one of those users to have made the annotation. This option is only applicable in combination with type `h`.
    * `<schemas>` is a comma-separated list of schema ids wrapped in `[` and `]` (in case there are multiple annotation schemes with the same key)
    * `<scopes>` is a comma-separated list of assignment scope ids (or bot_annotation_metadata) wrapped in `[` and `]` that these annotations have to be related to.

## Example queries
Get all papers published after 2020 or mentioning "carbon" in the title that were imported from Scopus (in this project, scopus was imported in two batches).
```
import: 689d6b84-fa26-4d24-8d8c-f13a13481a45 OR
        530e46de-7d0e-4954-a787-2f666742a6a4
AND (
  PY: >2020
  OR
  title: carbon
)
```

Get all papers that at least one person annotated as relevant (in this project, relevance is comprised of three labels).
``
annotation('cp', h, =1) AND annotation('imp', h, =1) AND annotation('exp', h, =1)
``

Get all papers with a resolved annotation in one of these two resolution scopes.
``
annotation('rel', r, true, scope=[196b7e05-cfe6-4a9d-b7ec-6a1bae865c2f, 0f49998c-25a5-4451-8af7-98f7f72c3fd7])
``

## `nacsos_data` example code
In some cases, using NQL might even be easier than writing SQL/SQLAlchemy queries.
Since the SQL parser and translater are implemented in the `nacsos_data` library, you can easily use it directly.
Here is an example on how to query the database, see the parse tree, the prepared SQL statement, and count results.

```python
from sqlalchemy.orm import Session
from nacsos_data.db import get_engine
from nacsos_data.db.crud.items.query.translate import Query

query = Query('YR: >=2000 AND <2005'
              'AND '
              '  ABSTRACT: CCS '
              'OR '
              'anno(exp, h, =1, ANY user IN [cc5da96f-25d5-48c2-8753-a837239429bc,'
              '                              433c8395-0bb9-491c-9325-85f589c27157])'
              , project_id='748e739d-f011-44de-9cb0-c9cb4bb18d08')
print('Q:', query)
print('----')
print(query.pretty)
print('----')
print(query.stmt)
print('----')

db_engine = get_engine(conf_file='/home/tim/workspace/nacsos-core/config/remote.env')
with db_engine.session() as session:  # type: Session
    LIMIT = 10
    n_docs = query.count(session=session)
    print(f'Query leads to {n_docs:,} hits')
    print(f'Listing the first {LIMIT}:')
    docs = query.results(session=session, limit=LIMIT)
    for doc in docs:
        print(doc)
```
produces
```text
Q: YR: >=2000 AND <2005 AND ABSTRACT: CCS OR anno(exp, h, =1, ANY user IN [cc5da96f-25d5-48c2-8753-a837239429bc,433c8395-0bb9-491c-9325-85f589c27157])
----
and
  py_filter
    and
      uint_clause
        >=
        2000
      uint_clause
        <
        2005
  or
    abstract_filter	CCS
    annotation_filter
      anno_params
        key	exp
        ltype	h
        value_int
          int_clause
            =
            1
        users
          ANY
          uuids
            cc5da96f-25d5-48c2-8753-a837239429bc
            433c8395-0bb9-491c-9325-85f589c27157
----
SELECT DISTINCT academic_item.item_id, item.item_id AS item_id_1, item.text, item.type, academic_item.project_id, item.project_id AS project_id_1, academic_item.doi, academic_item.wos_id, academic_item.scopus_id, academic_item.openalex_id, academic_item.s2_id, academic_item.pubmed_id, academic_item.dimensions_id, academic_item.title, academic_item.title_slug, academic_item.publication_year, academic_item.source, academic_item.keywords, academic_item.authors, academic_item.meta 
FROM item JOIN academic_item ON item.item_id = academic_item.item_id JOIN annotation AS annotation_1 ON academic_item.item_id = annotation_1.item_id JOIN annotation AS annotation_2 ON academic_item.item_id = annotation_2.item_id 
WHERE academic_item.project_id = :project_id_2 AND academic_item.publication_year >= :publication_year_1 AND academic_item.publication_year < :publication_year_2 AND (lower(item.text) LIKE lower(:text_1) OR annotation_1.key = :key_1 AND annotation_1.value_int = :value_int_1 AND annotation_1.user_id = :user_id_1 OR annotation_2.key = :key_2 AND annotation_2.value_int = :value_int_2 AND annotation_2.user_id = :user_id_2)
----
Query leads to 4 hits
Listing the first 10:
item_id=UUID('206e67c8-5ca5-4ba9-b5c8-41e53e72584f') ...
item_id=UUID('2a81a013-e959-4fce-a49a-394ae00e3921') ...
item_id=UUID('8986a4a6-dea7-41e3-8737-df3fc6259e07') ...
item_id=UUID('c98d0e35-7be5-4789-a326-b2917b3be1d0') ...
```