# Roadmap
Some known issues and bigger features that we are planning to work on:

* Generate prioritised label assignments using basic machine learning models directly on the platform
* Improve platform query parsing and make it available everywhere for a unified way to select data for assignments, export, ...
* Make deduplication and import overlap more transparent

*Last updated: 27.10.2023*