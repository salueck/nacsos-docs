# News
Here you can find the latest releases and changes to the platform.

## Release notes

### 01.11.2023
* Add button to send emails to users from the assignment scope config view to inform/remind them that they have open assignments
* Add button in user management view to send welcome mail with initial password
* Add button in user management view to send password reset mail

### 27.10.2023
* Inter-rater-reliability metrics now available via Annotation -> Monitoring  (see [documentation](/user/annotation/quality.md))
* Fixed NQL issues with multiple clauses

### 13.10.2023
* Buscar stopping criterion now available via Annotation -> Progress (see [documentation](/user/annotation/quality.md))
* Items can now be shown in columns for better readability
* Fixed issues with saving resolved annotations
* Added LexisNexis support

### 29.08.2023
Ordering of assignments was sometimes inconsistent. 
The ordering is now fixed and the progress indicators now follow the proper order.

### 25.08.2023
Keyboard bindings: Users can now annotate faster and more comfortably by just using the keyboard (in most cases).
For further details, check out [the documentation](user/annotation/annotation.md).

### 18.08.2023
Indicators in the progress bar of the assignment view now reveal an identifying number users may refer to and take notes on that might help them when resolving annotaiton conflicts. 

### 17.08.2023
Fundamentally refactored handling of imports.
We started off with trying to generalise import helpers to allow for wide range of supported formats.
This added a lot of complexity and was confusing for users.
In favour of simplicity, we now have many duplicated code fragments but a more intuitive interface and adding new formats might become easier in the future.

### 10.08.2023
Introducing the [NACSOS Query Language (NQL)](user/data-queries.md).
The NQL is an intuitive but powerful way to describe which articles of a project's dataset a user likes to select.
At this point, most of the basic features of NQL are supported by the backend and you can use it with the `nacsos_data` library.
The webinterface exposes NQL capabilities in the search bar in the dataset view.
In the future, we are planning to replace most or all filters on the platform with a unified NQL entry tool that helps you find the IDs you may want to refer to.
This allows users to be very specific about what documents they want to sample from for the next batch of assignments or for exporting.

### 07.08.2023
We now integrated our self-hosted OpenAlex database.
Users with the correct permission are now able to search mirror in Solr directly on the platform to develop their query and check for possible alias terms.
The documentation now has [an extra page](user/import/openalex.md) on that.
Given a query, we also support direct import from OpenAlex.
Please use this responsibly, as wrong settings may cause you to import millions of documents into the NACSOS database.

### Before August 2023
Everything else you see.
